package edu.ntnu.idatt1002.k25;

/**
 * TaskHolder is a singleton class. It is given and holds a task for other classes to use
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.04.08
 * @since 2021.04.08
 */
public final class TaskHolder {

    private Task task;
    private final static TaskHolder INSTANCE = new TaskHolder();

    /**
     * The constructor in a singleton class is private
     */
    private TaskHolder() {}

    public static TaskHolder getInstance() {
        return INSTANCE;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return this.task;
    }
}