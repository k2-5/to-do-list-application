package edu.ntnu.idatt1002.k25;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Persists data from the application to disk.
 *
 * @author Aurora Schmidt-Brekken Vollvik
 * @version 1.0.0 2021.04.02
 * @since 2021.04.02
 */
class FileHandler {

    private static final Path DATA_ROOT = Paths.get("data");
    private static final Path TASK_REGISTER_PATH = Paths.get(DATA_ROOT.toString(), "task-register.data");
    private static final Path CATEGORY_REGISTER_PATH = Paths.get(DATA_ROOT.toString(), "category-register.data");

    /**
     * Stores TaskRegister data to disk.
     *
     * @param tasks
     * @throws IOException
     */
    public static void serializeTaskRegister(ArrayList<Task> tasks) throws IOException {

        if (!Files.exists(DATA_ROOT)) {
            Files.createDirectory(DATA_ROOT);
        }

        try (FileOutputStream fs = new FileOutputStream(TASK_REGISTER_PATH.toFile());
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(tasks);
        }
    }

    /**
     * Reads TaskRegister data from disk.
     *
     * @return the TaskRegister object that was stored on disk, or null if the file is not readable.
     * @throws IOException
     */
    public static ArrayList<Task> deserializeTaskRegister() throws IOException {

        if (Files.isReadable(TASK_REGISTER_PATH)) {

            try (FileInputStream fs = new FileInputStream(TASK_REGISTER_PATH.toFile());
                 ObjectInputStream os = new ObjectInputStream(fs)) {

                return (ArrayList<Task>) os.readObject();

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Stores CategoryRegister data to disk.
     *
     * @param categories
     * @throws IOException
     */
    public static void serializeCategoryRegister(ArrayList<Category> categories) throws IOException {

        if (!Files.exists(DATA_ROOT)) {
            Files.createDirectory(DATA_ROOT);
        }

        try (FileOutputStream fs = new FileOutputStream(CATEGORY_REGISTER_PATH.toFile());
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(categories);
        }
    }

    /**
     * Reads CategoryRegister data from disk.
     *
     * @return the CategoryRegister object that was stored on disk, or null if the file is not readable.
     * @throws IOException
     */
    public static ArrayList<Category> deserializeCategoryRegister() throws IOException {

        if (Files.isReadable(CATEGORY_REGISTER_PATH)) {

            try (FileInputStream fs = new FileInputStream(CATEGORY_REGISTER_PATH.toFile()); ObjectInputStream os = new ObjectInputStream(fs)) {
                return (ArrayList<Category>) os.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
