package edu.ntnu.idatt1002.k25;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for handling all filter logic
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.04.14
 * @since 2021.03.11
 */
public class FilterHandler {

    /**
     * Returns the list of all tasks from TaskRegistered filtered according to FilterRegisters filter.
     * @return ArrayList<Task>
     */
    public static ArrayList<Task> applyFilter() {

        Filter filter = Filter.getFilter();

        Stream<Task> taskStream = TaskRegister.getTasks().stream();

        if(filter.isStatus()){
            ArrayList<String> statuses = new ArrayList<>();
            if(filter.isNotStarted()) statuses.add("Not started");
            if(filter.isInProgress()) statuses.add("In progress");
            if(filter.isFinished()) statuses.add("Finished");
            taskStream = taskStream.filter(task -> statuses.contains(task.getStatus()));
        }
        if(filter.isPriority()){
            if(filter.isLess()){
                taskStream = taskStream.filter(task -> task.getPriority() <= filter.getPriorityNumber());
            }else{
                taskStream = taskStream.filter(task -> task.getPriority() >= filter.getPriorityNumber());
            }
        }
        if(filter.isCategory()){
            taskStream = taskStream.filter(task -> filter.getCategories().contains(task.getCategory()));
        }
        if(filter.isDeadline() && filter.getDeadlineFirst() != null){
            taskStream = taskStream.filter(task -> !(task.getDeadline().isBefore(filter.getDeadlineFirst()) || task.getDeadline().isAfter(filter.getDeadlineLast())));
        }
        if(filter.isStartdate() && filter.getStartdateFirst() != null){
            taskStream = taskStream.filter(task -> !(task.getStartDate().isBefore(filter.getStartdateFirst()) || task.getStartDate().isAfter(filter.getStartdateLast())));
        }
        if(filter.isFinishdate() && filter.getFinishdateFirst() != null){
            taskStream = taskStream.filter(task -> !(task.getFinishDate().isBefore(filter.getFinishdateFirst()) || task.getFinishDate().isAfter(filter.getFinishdateLast())));
        }

        return taskStream.collect(Collectors.toCollection(ArrayList::new));
    }

}
