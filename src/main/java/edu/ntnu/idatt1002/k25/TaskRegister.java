package edu.ntnu.idatt1002.k25;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The register over all the available tasks.
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Oskar Løvstad Remvang
 * @version 1.0.0 2021.03.29
 * @since 2021.03.11
 */
public class TaskRegister implements Serializable {
    private static ArrayList<Task> tasks;


    private TaskRegister() {
        throw new IllegalStateException("This is a utility class");
    }

    /**
     * Initializes the tasks ArrayList if necessary, then returns it.
     *
     * @return the tasks ArrayList
     */
    public static ArrayList<Task> getTasks() {
        if (tasks == null) {
            load();
        }

        return tasks;
    }

    /**
     * Checks if task is registered.
     * Throw Exception if registered.
     * Adds task if not registered.
     * @param task
     * @throws Exception
     */
    public static void addTask(Task task) throws Exception {
        if (getTasks().contains(task)){
            throw new Exception("Task is already registered");
        }else {
            getTasks().add(task);
        }

        store();
    }

    /**
     * Loops through tasks and deletes the task if it is registered.
     * @param task
     */
    public static void deleteTask (Task task) throws Exception {
        if (!getTasks().contains(task)){
            throw new Exception("Task is not registered");
        } else {
            getTasks().remove(task);
        }

        store();
    }

    /**
     * Called whenever the register is updated to persist the changes.
     */
    private static void store() {

        try {
            FileHandler.serializeTaskRegister(tasks);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when the application starts to fill the register with data. Initializes the register with an empty
     * ArrayList if no data can be read from disk.
     *
     */
    private static void load() {

        try {
            ArrayList<Task> tasksFromDisk = FileHandler.deserializeTaskRegister();

            if (tasksFromDisk != null) {
                tasks = tasksFromDisk;
            } else {
                tasks = new ArrayList<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
