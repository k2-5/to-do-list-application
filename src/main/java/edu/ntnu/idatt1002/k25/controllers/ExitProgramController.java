package edu.ntnu.idatt1002.k25.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * The class for controlling the exit program popup window
 * @Author Gaute Degré Lorentsen
 * @version 1.0.0 2021.04.16
 * @since 2021.04.16
 */
public class ExitProgramController {
    @FXML
    public TextField textField;
    public Button noButton, yesButton;




    /**
     * Closes the stage if the no button is pressed.
     * Goes back to the previous window.
     * @param event
     * @throws IOException
     */
    public void cancelExit(ActionEvent event){
        Stage stage = (Stage) noButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Closes the application if the yes button is closed.
     * @param event
     */
    public void exitProgram(ActionEvent event) {
        Platform.exit();
    }
}
