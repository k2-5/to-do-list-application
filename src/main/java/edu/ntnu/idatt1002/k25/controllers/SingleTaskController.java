package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.Task;
import edu.ntnu.idatt1002.k25.TaskHolder;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;


/**
 * The class for controlling gui for a single task.
 *
 * @author Gaute Degré Lorentsen
 * @author Kim Johnstuen Rokling
 * @author Oskar Løvstad Remvang
 * @version 1.2.0 2021.04.09
 * @since 2021.03.31
 */
public class SingleTaskController {

    @FXML
    public TextField priorityText, statusText, categoryText, deadlineText, startDateText, endDateText, taskNameText;
    public Button editButton;
    public TextArea descriptionTextArea;

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void initialize() {
        Task task = TaskHolder.getInstance().getTask();
        taskNameText.setText(task.getTaskName());
        priorityText.setText(String.valueOf(task.getPriority()));
        statusText.setText(task.getStatus());
        categoryText.setText(task.getCategory().getCategoryName());
        deadlineText.setText((task.getDeadline() == null) ? "" : task.getDeadline().toString());
        startDateText.setText((task.getStartDate() == null) ? "" : task.getStartDate().toString());
        endDateText.setText((task.getFinishDate() == null) ? "" : task.getFinishDate().toString());
        descriptionTextArea.setText(task.getDescription());
    }

    /**
     * Method for button to take to new task.
     * @param event
     * @throws IOException
     */
    public void newTaskButton(ActionEvent event) throws IOException {
        loadScreen(event,"/NewTask.fxml");
    }

    /**
     * Method for button to take to categories.
     * @param event
     * @throws IOException
     */
    public void categories(ActionEvent event) throws IOException{
        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for button to take to the homes screen.
     * @param event
     * @throws IOException
     */
    public void goHome(ActionEvent event) throws IOException{
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for button to take to edit task
     * @param event
     * @throws IOException
     */
    public void editButton(ActionEvent event) throws IOException{
        loadScreen(event,"/EditTask.fxml");
    }

    /**
     * Method for opening popup window with verification when the user wants to exit the application.
     * @param event
     * @throws IOException
     */
    public void exitProgram(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/ExitProgramWarning.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }
}
