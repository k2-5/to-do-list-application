package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.Category;
import edu.ntnu.idatt1002.k25.CategoryRegister;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

/**
 * The class for controlling gui for category.
 *
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Oskar Løvstad Remvang
 * @version 1.1.0 2021.04.09
 * @since 2021.04.01
 */
public class AddCategoryController {
    @FXML
    public Button saveButton, cancelButton;
    public TextField categoryNameText;

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Closes the add menu
     * @param event
     */
    private void closeAddMenu(Event event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Method for button for saving category.
     * Opens a popup window if category is already registered warning the user.
     * @param event
     * @throws IOException
     */
    public void saveCategory(ActionEvent event) throws Exception {
        Category newCategory = new Category(categoryNameText.getText());

        //Opens a popup window if category is already registered warning the user.
        if (CategoryRegister.getCategories().contains(newCategory)){
            Parent root = FXMLLoader.load(getClass().getResource("/AlreadyRegistered.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setTitle("My to-do list");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();
            categoryNameText.setText("");
        }

        closeAddMenu(event);
        CategoryRegister.addCategory(newCategory);
    }

    /**
     * Method for button for canceling category.
     * @param event
     * @throws IOException
     */
    public void cancelCategory(ActionEvent event) throws IOException{
        closeAddMenu(event);
    }

}
