package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.Category;
import edu.ntnu.idatt1002.k25.CategoryRegister;
import edu.ntnu.idatt1002.k25.TaskRegister;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.Optional;

/**
 * The class for controlling gui for category.
 *
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Oskar Løvstad Remvang
 * @author Kim Johnstuen Rokling
 * @version 1.1.1 2021.04.19
 * @since 2021.03.31
 */
public class CategoryController {
    @FXML
    public ListView categoryText;
    public Button addButton, editCategoryButton, deleteButton;

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public void initialize(){
        /**
         * Adds keylistener for deleting category by marking it and pressing DELETE
         */
        categoryText.setOnKeyPressed(event -> {
            if(event.getCode().equals(KeyCode.DELETE)){
                try {
                    deleteCategoryKeyEvent(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        loadCategories();
    }

    /**
     * Loads the catergories to the listView
     */
    private void loadCategories() {
        categoryText.getItems().clear();
        categoryText.getItems().addAll(CategoryRegister.getCategories());
    }

    public void deleteCategoryKeyEvent(KeyEvent event) throws Exception {
        if (categoryText.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
            alert.setHeaderText("Delete " + categoryText.getSelectionModel().getSelectedItem() + "?");
            alert.setResizable(false);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES){
                CategoryRegister.deleteCategory((Category) categoryText.getSelectionModel().getSelectedItem());
                loadScreen(event,"/Category.fxml");
            }


            //Sets the tasks with the deleted category to uncategorized
            TaskRegister.getTasks().stream().filter(task -> task.getCategory().equals(categoryText.getSelectionModel().getSelectedItem()))
                    .forEach(task -> task.getCategory().setCategoryName("Uncategorized"));
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a category", ButtonType.OK);
            alert.setResizable(false);
            alert.show();
        }
    }


    /**
     * Method for button to take to new task.
     * @param event
     * @throws IOException
     */
    public void newTaskButton(ActionEvent event) throws IOException {
        loadScreen(event,"/NewTask.fxml");
    }

    /**
     * Method for button to take to categories.
     * @param event
     * @throws IOException
     */
    public void categories(ActionEvent event) throws IOException{
        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for button to take to the homes screen.
     * @param event
     * @throws IOException
     */
    public void goHome(ActionEvent event) throws IOException{
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Adds a new category
     * @param event
     * @throws IOException
     */
    public void addCategory(ActionEvent event) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/AddCategory.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("My to-do list");
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();
        loadCategories();
    }

    /**
     * Deletes the category the user wants to delete
     * @param event
     * @throws Exception
     */
    public void deleteCategory(ActionEvent event) throws Exception {
        if (categoryText.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
            alert.setHeaderText("Delete " + categoryText.getSelectionModel().getSelectedItem() + "?");
            alert.setResizable(false);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES){
                CategoryRegister.deleteCategory((Category) categoryText.getSelectionModel().getSelectedItem());
                loadScreen(event,"/Category.fxml");
            }


            //Sets the tasks with the deleted category to uncategorized
            TaskRegister.getTasks().stream().filter(task -> task.getCategory().equals(categoryText.getSelectionModel().getSelectedItem()))
                    .forEach(task -> task.getCategory().setCategoryName("Uncategorized"));
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a category", ButtonType.OK);
            alert.setResizable(false);
            alert.show();
        }
    }

    /**
     * Creates a popup window where the user can put in a new name for the category
     * Then it will call the editCategoryName from CategoryRegister
     * @param event
     * @throws Exception
     */
    public void editCategory(ActionEvent event) throws Exception {
        TextInputDialog td = new TextInputDialog();
        td.setHeaderText("Enter the new category name");
        Button d = new Button("Save");
        Optional<String> newCatName = td.showAndWait();

        // Checks if field is not empty nor blank. newCatName would throw exception otherwise if called and it is are empty or blank.
        if (!newCatName.isEmpty() && !newCatName.get().isBlank()) {
            CategoryRegister.editCategoryName((Category) categoryText.getSelectionModel().getSelectedItem(), newCatName.get());
        }

        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for opening popup window with verification when the user wants to exit the application.
     * @param event
     * @throws IOException
     */
    public void exitProgram(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/ExitProgramWarning.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }
}
