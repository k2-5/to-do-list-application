package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.Category;
import edu.ntnu.idatt1002.k25.CategoryRegister;
import edu.ntnu.idatt1002.k25.Task;
import edu.ntnu.idatt1002.k25.TaskRegister;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

/**
 * The class for controlling gui for new Task.
 *
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Kim Johnstuen Rokling
 * @author Oskar Løvstad Remvang
 * @version 1.2.2 2021.04.19
 * @since 2021.03.31
 */
public class NewTaskController {

    @FXML
    public TextArea descriptionTextArea;
    public TextField taskNameText;
    public ChoiceBox statusChoiceBox, categoryChoiceBox;
    public Button saveButton, cancelButton;
    public DatePicker deadlineDate, endDatePicker, startDatePicker;
    public Spinner<Integer> prioritySpinner;

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public void initialize() {
        statusChoiceBox.getItems().addAll("Not started", "In progress", "Finished");
        statusChoiceBox.setValue("Not started");

        categoryLoader();

        prioritySpinner.setPromptText("Priority");
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,6);
        valueFactory.setValue(1);
        prioritySpinner.setValueFactory(valueFactory);


    }

    /**
     * Able to add a new category while adding a new task
     * @param event
     * @throws IOException
     */
    public void newCategory(ActionEvent event) throws IOException {
        categoryChoiceBox.getItems().clear();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/AddCategory.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("My to-do list");
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();

        categoryLoader();
    }

    /**
     * Loads the category into the choice box
     */
    private void categoryLoader() {
        Category uncategorized = new Category("Uncategorized");
        categoryChoiceBox.getItems().add(uncategorized);
        categoryChoiceBox.getItems().addAll(CategoryRegister.getCategories());
        categoryChoiceBox.setValue(uncategorized);
    }


    /**
     * Method for button to take to new task.
     * @param event
     * @throws IOException
     */
    public void newTaskButton(ActionEvent event) throws IOException {
        loadScreen(event,"/NewTask.fxml");
    }

    /**
     * Method for button to take to categories.
     * @param event
     * @throws IOException
     */
    public void categories(ActionEvent event) throws IOException{
        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for button to take to the home screen.
     * @param event
     * @throws IOException
     */
    public void goHome(ActionEvent event) throws IOException{
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for cancelbutton to cancel adding task.
     * @param event
     * @throws IOException
     */
    public void cancelButton(ActionEvent event) throws IOException{
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Creates a new task and adds it to the ArrayList in TaskRegister
     * Opens a popup window if tasks is already registered warning the user.
     * @param event
     * @throws Exception
     */
    public void save(ActionEvent event) throws Exception {
        Task task = new Task(taskNameText.getText(), deadlineDate.getValue(), startDatePicker.getValue(), endDatePicker.getValue(),
                statusChoiceBox.getValue().toString(), descriptionTextArea.getText(),prioritySpinner.getValue().byteValue(), (Category) categoryChoiceBox.getValue());

        //Opens a popup window if tasks is already registered warning the user.
        if (TaskRegister.getTasks().contains(taskNameText.getText())){
            Parent root = FXMLLoader.load(getClass().getResource("/AlreadyRegistered.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setTitle("My to-do list");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();
        }
        TaskRegister.addTask(task);

        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for opening popup window with verification when the user wants to exit the application.
     * @param event
     * @throws IOException
     */
    public void exitProgram(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/ExitProgramWarning.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }


}

