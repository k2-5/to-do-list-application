package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.Category;
import edu.ntnu.idatt1002.k25.CategoryRegister;
import edu.ntnu.idatt1002.k25.Filter;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * The class for controlling gui for the Filter Menu.
 * @author Oskar Løvstad Remvang
 * @version 1.1.2 2021.04.14
 * @since 2021.03.31
 */
public class FilterController {

    @FXML
    public CheckBox status, priority, category,deadline,startdate,finishdate;
    public CheckBox notStarted, inProgress, finished;
    public RadioButton less, greater;
    public Spinner<Integer> prioritySpinner;
    public DatePicker deadlineFirst, deadlineLast, startdateFirst, startdateLast,
    finishdateFirst, finishdateLast;
    public Text betweenDatesDeadline, betweenDatesStartdate, betweenDatesFinishdate;
    public Text firstDateDeadline, lastDateDeadline, firstDateStartdate, lastDateStartdate, firstDateFinishdate, lastDateFinishdate;
    public ScrollPane categoriesScrollPane;

    private final ArrayList<RadioButton> categoryButtons = new ArrayList<>();
    private final Filter filter = Filter.getFilter();

    /**
     * Closes the filtermenu
     * @param event
     */
    private void closeFilterMenu(Event event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Method to initialize the filter menu. Sets visibility and assigns values to all fields.
     * @throws Exception
     */
    public void initialize() throws Exception{
        //Creates the spinner for priority
        prioritySpinner.setPromptText("Priority");
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,6);
        valueFactory.setValue(1);
        prioritySpinner.setValueFactory(valueFactory);

        //Allows only either the less or the greater RadioButton to be checked of at once
        ToggleGroup group = new ToggleGroup();
        less.setToggleGroup(group);
        greater.setToggleGroup(group);

        setPreviousFilterValues();

        hideNodesUnlessChecked();

        addCategories();
    }

    /**
     * Adds all categories in CategiryRegister to filter menu.
     * Sets visibility and selected value according to filter choices
     */
    private void addCategories(){
        HBox scrollPaneBox = new HBox();
        categoriesScrollPane.setContent(scrollPaneBox);
        ArrayList<Category> categories = CategoryRegister.getCategories();
        for(Category category : categories){
            RadioButton button = new RadioButton();
            if(!filter.isCategory()) button.setVisible(false);
            button.setText(category.getName());
            if(filter.getCategories().contains(category)) button.setSelected(true);
            scrollPaneBox.getChildren().add(button);
            categoryButtons.add(button);
        }
    }

    /**
     * Sets values of the nodes according to the previous filter choices
     */
    private void setPreviousFilterValues(){
        status.setSelected(filter.isStatus());
        priority.setSelected(filter.isPriority());
        category.setSelected(filter.isCategory());
        if(filter.getDeadlineFirst()!=null) deadline.setSelected(filter.isDeadline());
        if(filter.getStartdateFirst()!=null) startdate.setSelected(filter.isStartdate());
        if(filter.getFinishdateFirst()!=null) finishdate.setSelected(filter.isFinishdate());
        notStarted.setSelected(filter.isNotStarted());
        inProgress.setSelected(filter.isInProgress());
        finished.setSelected(filter.isFinished());
        greater.setSelected(filter.isGreater());
        less.setSelected(filter.isLess());
        prioritySpinner.getValueFactory().setValue(filter.getPriorityNumber());

        deadlineLast.setDisable(true);
        startdateLast.setDisable(true);
        finishdateLast.setDisable(true);
        if(filter.getDeadlineFirst()!=null) {
            deadlineFirst.setValue(filter.getDeadlineFirst());
            deadlineLast.setValue(filter.getDeadlineLast());
            deadlineLast.setDisable(false);
        }
        if(filter.getStartdateFirst()!=null) {
            startdateFirst.setValue(filter.getStartdateFirst());
            startdateLast.setValue(filter.getStartdateLast());
            startdateLast.setDisable(false);
        }
        if(filter.getFinishdateFirst()!=null) {
            finishdateFirst.setValue(filter.getFinishdateFirst());
            finishdateLast.setValue(filter.getFinishdateLast());
            finishdateLast.setDisable(false);
        }
    }

    /**
     * Sets visibility of all filter parameter nodes to false unless their respective filter-group is selected
     */
    private void hideNodesUnlessChecked(){
        if(!status.isSelected()){notStarted.setVisible(false); inProgress.setVisible(false); finished.setVisible(false); }
        if(!priority.isSelected()){less.setVisible(false); greater.setVisible(false); prioritySpinner.setVisible(false);}
        if(!deadline.isSelected()){deadlineFirst.setVisible(false); deadlineLast.setVisible(false); betweenDatesDeadline.setVisible(false);firstDateDeadline.setVisible(false); lastDateDeadline.setVisible(false);}
        if(!startdate.isSelected()){startdateFirst.setVisible(false); startdateLast.setVisible(false); betweenDatesStartdate.setVisible(false);firstDateStartdate.setVisible(false); lastDateStartdate.setVisible(false);}
        if(!finishdate.isSelected()){finishdateFirst.setVisible(false); finishdateLast.setVisible(false); betweenDatesFinishdate.setVisible(false);firstDateFinishdate.setVisible(false); lastDateFinishdate.setVisible(false);}
    }

    /**
     * Method assign to the cancel button of the filter menu. Returns to HomeScreen without doing anything else.
     * @param event
     * @throws IOException
     */
    public void cancel(ActionEvent event) throws IOException {
        closeFilterMenu(event);
    }

    /**
     * Returns a Filter object retaining all choices from the filter menu.
     * @return Filter
     */
    private void extractFilter(){
        ArrayList<String> namesOfSelectedCategories = categoryButtons.stream()
                                                    .filter(ToggleButton::isSelected)
                                                    .map(RadioButton::getText)
                                                    .collect(Collectors.toCollection(ArrayList::new));

        ArrayList<Category> selectedCategories = CategoryRegister.getCategories().stream()
                                                .filter(category -> namesOfSelectedCategories.contains(category.categoryName))
                                                .collect(Collectors.toCollection(ArrayList::new));

        Filter.setFilterValues(status.isSelected(), priority.isSelected(), category.isSelected(),
                deadline.isSelected(), startdate.isSelected(), finishdate.isSelected(), notStarted.isSelected(),
                inProgress.isSelected(),finished.isSelected(),less.isSelected(),greater.isSelected(),deadlineFirst.getValue(),
                deadlineLast.getValue(), startdateFirst.getValue(), startdateLast.getValue(), finishdateFirst.getValue(),
                finishdateLast.getValue(), prioritySpinner.getValue(),selectedCategories);
    }

    /**
     * Method for the Apply button in Filter Menu. Applies the filter currently displayed in the Filter Menu.
     * @param event
     * @throws IOException
     */
    public void apply(ActionEvent event) throws IOException {
        extractFilter();

        closeFilterMenu(event);
    }

    /**
     *CheckBox for Status. Toggles visibilty of all related Nodes. This is true for all of FilterControllers onToggle methods.
     * @param event
     * @throws IOException
     */
    public void onToggleStatus(ActionEvent event) throws IOException {
        notStarted.setVisible(!notStarted.isVisible());
        inProgress.setVisible(!inProgress.isVisible());
        finished.setVisible(!finished.isVisible());
    }

    public void onTogglePriority(ActionEvent event) throws IOException {
        less.setVisible(!less.isVisible());
        greater.setVisible(!greater.isVisible());
        prioritySpinner.setVisible(!prioritySpinner.isVisible());
    }

    public void onToggleCategory(ActionEvent event) throws IOException {
        for(RadioButton button : categoryButtons){
            button.setVisible(!button.isVisible());
        }
    }

    public void onToggleDeadline(ActionEvent event) throws IOException {
        betweenDatesDeadline.setVisible(!betweenDatesDeadline.isVisible());
        firstDateDeadline.setVisible(!firstDateDeadline.isVisible());
        lastDateDeadline.setVisible(!lastDateDeadline.isVisible());
        deadlineFirst.setVisible(!deadlineFirst.isVisible());
        deadlineLast.setVisible(!deadlineLast.isVisible());
    }

    public void onToggleStartdate(ActionEvent event) throws IOException {
        betweenDatesStartdate.setVisible(!betweenDatesStartdate.isVisible());
        firstDateStartdate.setVisible(!firstDateStartdate.isVisible());
        lastDateStartdate.setVisible(!lastDateStartdate.isVisible());
        startdateFirst.setVisible(!startdateFirst.isVisible());
        startdateLast.setVisible(!startdateLast.isVisible());
    }

    public void onToggleFinishdate(ActionEvent event) throws IOException {
        betweenDatesFinishdate.setVisible(!betweenDatesFinishdate.isVisible());
        firstDateFinishdate.setVisible(!firstDateFinishdate.isVisible());
        lastDateFinishdate.setVisible(!lastDateFinishdate.isVisible());
        finishdateFirst.setVisible(!finishdateFirst.isVisible());
        finishdateLast.setVisible(!finishdateLast.isVisible());
    }

    /**
     * Sets the date of the last datepicker equal to the first datepicker. True for all FilterControllers firstDate methods.
     * @param event
     * @throws IOException
     */
    public void firstDateDeadline(ActionEvent event) throws IOException{
        deadlineLast.setValue(deadlineFirst.getValue());
        deadlineLast.setDisable(false);
    }

    public void firstDateStartdate(ActionEvent event) throws IOException{
        startdateLast.setValue(startdateFirst.getValue());
        startdateLast.setDisable(false);
    }

    public void firstDateFinishdate(ActionEvent event) throws IOException{
        finishdateLast.setValue(finishdateFirst.getValue());
        finishdateLast.setDisable(false);
    }

    /**
     * Checks wether picked lastdate is valid or not. Sets it to firstdate if not.
     * True for all FilterControllers lastDate methods.
     * @param event
     * @throws IOException
     */
    public void lastDateDeadline(ActionEvent event) throws IOException{
        if(deadlineLast.getValue().isBefore(deadlineFirst.getValue())){
            deadlineLast.setValue(deadlineFirst.getValue());
        }
    }

    public void lastDateStartdate(ActionEvent event) throws IOException{
        if(startdateLast.getValue().isBefore(startdateFirst.getValue())){
            startdateLast.setValue(startdateFirst.getValue());
        }
    }

    public void lastDateFinishdate(ActionEvent event) throws IOException{
        if(finishdateLast.getValue().isBefore(finishdateFirst.getValue())){
            finishdateLast.setValue(finishdateFirst.getValue());
        }
    }
}
