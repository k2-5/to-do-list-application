package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.*;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;


/**
 * The class for controlling gui for edit Task.
 *
 * @author Gaute Degré Lorentsen
 * @author Kim Johnstuen Rokling
 * @author Oskar Løvstad Remvang
 * @version 1.3.1 2021.04.19
 * @since 2021.03.31
 */
public class EditTaskController {

    @FXML
    public TextArea descriptionTextArea;
    public TextField taskNameText;
    public ChoiceBox statusChoiceBox, categoryChoiceBox;
    public Button saveButton, cancelButton;
    public DatePicker deadlineDate, endDatePicker, startDatePicker;
    public Spinner<Integer> prioritySpinner;

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void initialize() {
        prioritySpinner.setPromptText("Priority");
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,6);
        valueFactory.setValue(1);
        prioritySpinner.setValueFactory(valueFactory);

        Task task = TaskHolder.getInstance().getTask();
        taskNameText.setText(task.getTaskName());
        prioritySpinner.getValueFactory().setValue((int)task.getPriority());
        deadlineDate.setValue(task.getDeadline());
        endDatePicker.setValue(task.getFinishDate());
        startDatePicker.setValue(task.getStartDate());
        descriptionTextArea.setText(task.getDescription());

        statusChoiceBox.getItems().addAll("Not started", "In progress", "Finished");
        statusChoiceBox.setValue(task.getStatus());

        categoryChoiceBox.getItems().addAll(CategoryRegister.getCategories());
        categoryChoiceBox.setValue(task.getCategory());
    }

    /**
     * Method for button to take to new task.
     * @param event
     * @throws IOException
     */
    public void newTaskButton(ActionEvent event) throws IOException {
        loadScreen(event,"/NewTask.fxml");
    }

    /**
     * Method for button to take to categories.
     * @param event
     * @throws IOException
     */
    public void categories(ActionEvent event) throws IOException{
        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for button to take to the homes screen.
     * @param event
     * @throws IOException
     */
    public void goHome(ActionEvent event) throws IOException{
        loadScreen(event,"/HomeScreen.fxml");
    }

    public void saveButton(ActionEvent event) throws IOException{
        Task task = TaskHolder.getInstance().getTask();

        // TODO should probably call a method in taskRegister that do these changes
        task.setTaskName(taskNameText.getText());
        task.setPriority(prioritySpinner.getValue().byteValue());
        task.setStatus(statusChoiceBox.getValue().toString());
        task.setCategory((Category) categoryChoiceBox.getValue());
        task.setDeadline(deadlineDate.getValue());
        task.setStartDate(startDatePicker.getValue());
        task.setFinishDate(endDatePicker.getValue());
        task.setDescription(descriptionTextArea.getText());

        loadScreen(event,"/SingleTask.fxml");
    }

    /**
     * Does not save any changes to the task, and returns to SingleTask
     * @param event
     * @throws IOException
     */
    public void cancelButton(ActionEvent event) throws IOException{
        loadScreen(event,"/SingleTask.fxml");
    }

    /**
     * Deletes the task, and returns to the homeScreen
     * @param event
     * @throws Exception
     */
    public void deleteButton(ActionEvent event) throws Exception {
        Task task = TaskHolder.getInstance().getTask();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Delete " + task.getTaskName() + "?");
        alert.setResizable(false);
        alert.showAndWait();

        if(alert.getResult() == ButtonType.YES) {
            TaskRegister.deleteTask(task);
            loadScreen(event,"/HomeScreen.fxml");
        }

    }

    /**
     * Method for opening popup window with verification when the user wants to exit the application.
     * @param event
     * @throws IOException
     */
    public void exitProgram(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/ExitProgramWarning.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Able to add a new category while adding editng task
     * @param event
     * @throws IOException
     */
    public void newCategory(ActionEvent event) throws IOException {
        categoryChoiceBox.getItems().clear();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/AddCategory.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("My to-do list");
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        stage.setScene(new Scene(root, width, height));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();

        categoryLoader();
    }

    /**
     * Loads the category into the choice box
     */
    private void categoryLoader() {
        Category uncategorized = new Category("Uncategorized");
        categoryChoiceBox.getItems().add(uncategorized);
        categoryChoiceBox.getItems().addAll(CategoryRegister.getCategories());
        categoryChoiceBox.setValue(uncategorized);
    }
}
