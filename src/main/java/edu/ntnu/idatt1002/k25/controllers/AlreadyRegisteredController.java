package edu.ntnu.idatt1002.k25.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * The class for controlling the popup window for already registered tasks/categories
 * @Author Gaute Degré Lorentsen
 * @version 1.0.0 2021.04.19
 * @since 2021.04.19
 */
public class AlreadyRegisteredController {
    @FXML
    public Button okBut;

    /**
     * Closes the popup window when ok-button is pressed
     * @param event
     */
    public void okButton(ActionEvent event){
        Stage stage = (Stage) okBut.getScene().getWindow();
        stage.close();

    }
}
