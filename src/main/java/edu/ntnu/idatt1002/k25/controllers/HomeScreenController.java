package edu.ntnu.idatt1002.k25.controllers;

import edu.ntnu.idatt1002.k25.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * The class for controlling gui for the Home screen.
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Kim Johnstuen Rokling
 * @author Oskar Løvstad Remvang
 * @version 1.2.4 2021.04.21
 * @since 2021.03.31
 */
public class HomeScreenController {

    @FXML
    public TableColumn<Task, String> taskNameTable, priorityTable, statusTable, categoryTable,
            deadlineTable, startDateTable, finishDateTable;
    public CheckBox filterCheckBox;
    public TextField searchField;
    public Button searchButton;
    public TableView<Task> tableView;
    public Button myTasksButton, newTaskButton, categoriesButton;

    private static boolean filterIsChecked;
    private static boolean isSearching;
    private static String searchName;

    private ObservableList<Task> taskList = FXCollections.observableArrayList();

    /**
     * Loads a scene
     * @param event
     * @throws IOException
     */
    private void loadScreen(Event event, String screen) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(screen));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Scene scene = new Scene(root, width, height);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Method to initialize the home screen. Adds tasks to table.
     * @throws Exception
     */
    public void initialize() throws Exception {
        /**
         * Clears list of tasks.
         */
        taskList.clear();

        /**
         * Adds all the tasks in TaskRegister to the taskList if it is not empty.
         */
        taskList.addAll(TaskRegister.getTasks());

        /**
         * Applies filter
         */
        applyFilter();

        searchField();

        /**
         * Adds the different parameters to their correct column in the table
         */
        taskNameTable.setCellValueFactory(new PropertyValueFactory<>("taskName"));
        priorityTable.setCellValueFactory(new PropertyValueFactory<>("priority"));
        statusTable.setCellValueFactory(new PropertyValueFactory<>("status"));
        categoryTable.setCellValueFactory(new PropertyValueFactory<>("category"));
        deadlineTable.setCellValueFactory(new PropertyValueFactory<>("deadline"));
        startDateTable.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        finishDateTable.setCellValueFactory(new PropertyValueFactory<>("finishDate"));
        tableView.setItems(taskList);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        mouseListener();
        keyListener();


    }

    /**
     * Adds a MouseListener to each non empty row in order to open tasks by double clicks
     * Deletes a task by marking it and clicking DELETE on keyboard
     * Opens a task by marking it and clicking SPACE on keyboard
     */
    private void mouseListener() {
        tableView.setRowFactory(tv -> {
            TableRow<Task> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty() ) {
                    getTaskFromTable();

                    try {
                        loadScreen(event, "/SingleTask.fxml");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (event.getClickCount() == 1 && !row.isEmpty() ) {
                    getTaskFromTable();
                    tableView.setOnKeyPressed(ev -> {
                        if(ev.getCode().equals(KeyCode.DELETE)){
                            try {
                                deleteButton(ev);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (ev.getCode().equals(KeyCode.SPACE)){
                            try {
                                loadScreen(event, "/SingleTask.fxml");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
            return row ;
        });


    }

    /**
     * Adds a keyListener that opens a task when marked with enter and presses space.
     * Deletes a task on DELETE.
     */
    private void keyListener(){
        tableView.setOnKeyPressed(tv -> {
            if (tv.getCode().equals(KeyCode.DELETE)){
                getTaskFromTable();
                try {
                    deleteButton(tv);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (tv.getCode().equals(KeyCode.SPACE)){
                getTaskFromTable();
                try {
                    loadScreen(tv, "/SingleTask.fxml");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Get the selected task from the table and saves it in task holder.
     */
    private void getTaskFromTable() {
        TaskHolder taskHolder = TaskHolder.getInstance();
        taskHolder.setTask(tableView.getSelectionModel().getSelectedItem());
    }

    /**
     * Method for the search field
     */
    private void searchField() {
        /**
         * Adds keylistener for enter-key to the searchfield
         */
        searchField.setOnKeyPressed(event -> {
            if(event.getCode().equals(KeyCode.ENTER)){
                try {
                    searchForTask(event);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        /**
         * Shows tasks that are searched for
         */
        searchField.setText(searchName);
        if(isSearching){
            if(searchName != null){
                ArrayList<Task> tasks = TaskRegister.getTasks().stream()
                        .filter(task -> task.getTaskName().toLowerCase().contains(searchName.toLowerCase()))
                        .collect(Collectors.toCollection(ArrayList::new));
                taskList.clear();
                taskList.addAll(tasks);
            }
            isSearching = false;
        }
    }

    /**
     * Applies filter
     */
    private void applyFilter() {
        filterCheckBox.setSelected(filterIsChecked);
        if(filterIsChecked){
            taskList.clear();
            taskList.addAll(FilterHandler.applyFilter());
        }
    }

    /**
     * Able to delete a task by marking it and press DELETE
     * @param event
     * @throws Exception
     */
    public void deleteButton(KeyEvent event) throws Exception {
        Task task = TaskHolder.getInstance().getTask();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Delete " + task.getTaskName() + "?");
        alert.setResizable(false);
        alert.showAndWait();

        if(alert.getResult() == ButtonType.YES) {
            TaskRegister.deleteTask(task);
        }

        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for button to take to new task.
     * @param event
     * @throws IOException
     */
    public void newTaskButton(ActionEvent event) throws IOException {
        loadScreen(event,"/NewTask.fxml");
    }

    /**
     * Method for button to take to categories.
     * @param event
     * @throws IOException
     */
    public void categories(ActionEvent event) throws IOException {
        loadScreen(event,"/Category.fxml");
    }

    /**
     * Method for button to take to the homes screen.
     * @param event
     * @throws IOException
     */
    public void goHome(ActionEvent event) throws IOException {
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for button to open a single task
     * @param event
     * @throws IOException
     */
    public void openTask(ActionEvent event) throws IOException {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            getTaskFromTable();

            loadScreen(event,"/SingleTask.fxml");
        }
    }


    /**
     * Method for button to open Filter Menu
     * @param event
     * @throws IOException
     */
    public void filter(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Filter.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("My to-do list");
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.showAndWait();
        setFilterChecked(true);
        filterCheckBox.setSelected(filterIsChecked);
        applyFilter();

    }

    /**
     * Method for CheckBox for turning filtering on and off
     * @param event
     * @throws IOException
     */
    public void filterCheckBox(ActionEvent event) throws IOException {
        filterIsChecked = filterCheckBox.isSelected();

        loadScreen(event,"/HomeScreen.fxml");
    }

    public static void setFilterChecked(boolean checked){
        filterIsChecked = checked;
    }

    public void searchForTask(Event event) throws IOException {
        isSearching = true;
        searchName = searchField.getText();
        loadScreen(event,"/HomeScreen.fxml");
    }

    /**
     * Method for opening popup window with verification when the user wants to exit the application.
     * @param event
     * @throws IOException
     */
    public void exitProgram(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/ExitProgramWarning.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setTitle("My to-do list");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();

    }
}
