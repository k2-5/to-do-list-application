package edu.ntnu.idatt1002.k25;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * The register over all the available categories.
 *
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @version 1.0.1 2021.03.29
 * @since 2021.03.11
 */
public class CategoryRegister implements Serializable {
    private static ArrayList<Category> categories;

    private CategoryRegister() {
        throw new IllegalStateException("This is a utility class");
    }

    /**
     * Checks if the new category already is in the category register.
     * Throws exception if category exists.
     * Adds new category if it doesn't exist.
     * @param category
     * @throws Exception
     */
    public static void addCategory(Category category) throws Exception {
        if(!getCategories().contains(category)){
            getCategories().add(category);
        } else {
            throw new Exception("Category is already registered");
        }

        storeCategories();
    }

    /**
     * Checks if the new category already is in the category register.
     * Throws exception if category does not exist.
     * Delete category if it exist.
     * @param category
     * @throws Exception
     */
    public static void deleteCategory(Category category) throws Exception {
        if(!getCategories().contains(category)){
            throw new Exception("Category not in list");
        }else {
            getCategories().remove(category);
        }

        storeCategories();
    }

    /**
     * Changes the name of an already registered category
     * @param category
     * @param newName - the name you want the category to be change to
     * @throws Exception
     */
    public static void editCategoryName(Category category, String newName) throws Exception {
        category.setCategoryName(newName);

        storeCategories();
    }

    /**
     * Initializes the categories ArrayList if necessary, then returns it.
     * @return the categories ArrayList
     */
    public static ArrayList<Category> getCategories() {
        if (categories == null) {
            loadCategories();
        }
        return categories;
    }



    /**
     * Called whenever the register is updated to persist the data.
     */
    private static void storeCategories() {

        try {
            FileHandler.serializeCategoryRegister(categories);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when the application starts to fill the register with data. Initializes the register with an empty
     * ArrayList if no data can be read from disk.
     */
    public static void loadCategories() {

        try {
            ArrayList<Category> categoriesFromDisk = FileHandler.deserializeCategoryRegister();

            if (categoriesFromDisk != null) {
                categories = categoriesFromDisk;
            } else {
                categories = new ArrayList<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
