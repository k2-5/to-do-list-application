package edu.ntnu.idatt1002.k25;


import java.io.Serializable;
import java.util.Objects;


/**
 * The class for a single category.
 *
 * @author Gaute Degré Lorentsen
 * @author Aurora Schmidt-Brekken Vollvik
 * @version 1.0.1 2021.03.29
 * @since 2021.03.11
 */
public class Category implements Serializable {
    public String categoryName;

    /**
     *
     * @param name - Sets name of Category
     */
    public Category(String name){

        categoryName = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String newCategoryName) {
        categoryName = newCategoryName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(categoryName, category.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryName);
    }

    public void setName(String name) {
        this.categoryName = name;
    }

    public String getName() {
        return categoryName;
    }



    @Override
    public String toString() {
        return categoryName;
    }
}
