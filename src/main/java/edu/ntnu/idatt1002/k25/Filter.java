package edu.ntnu.idatt1002.k25;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * The class for a single task.
 *
 * @author Oskar Løvstad Remvang
 * @version 1.0.1 2021.04.14
 * @since 2021.03.11
 */
public class Filter {

    private static final Filter filter = new Filter(false,false,false,false,false,false,true,true,true, false, true, null, null,null,null,null,null,1,CategoryRegister.getCategories());

    private boolean status, priority, category, deadline, startdate, finishdate;
    private boolean notStarted, inProgress, finished;
    private int priorityNumber;
    private boolean less, greater;
    private LocalDate deadlineFirst, deadlineLast, startdateFirst, startdateLast, finishdateFirst, finishdateLast;
    private ArrayList<Category> categories;

    private Filter(boolean status, boolean priority, boolean category, boolean deadline, boolean startdate, boolean finishdate, boolean notStarted, boolean inProgress, boolean finished, boolean less, boolean greater, LocalDate deadlineFirst, LocalDate deadlineLast, LocalDate startdateFirst, LocalDate startdateLast, LocalDate finishdateFirst, LocalDate finishdateLast, int priorityNumber, ArrayList<Category> categories) {
        this.status = status;
        this.priority = priority;
        this.category = category;
        this.deadline = deadline;
        this.startdate = startdate;
        this.finishdate = finishdate;
        this.notStarted = notStarted;
        this.inProgress = inProgress;
        this.finished = finished;
        this.less = less;
        this.greater = greater;
        this.deadlineFirst = deadlineFirst;
        this.deadlineLast = deadlineLast;
        this.startdateFirst = startdateFirst;
        this.startdateLast = startdateLast;
        this.finishdateFirst = finishdateFirst;
        this.finishdateLast = finishdateLast;
        this.priorityNumber = priorityNumber;
        this.categories = categories;
    }

    public static Filter getFilter(){
        return filter;
    }

    public static void setFilterValues(boolean status, boolean priority, boolean category, boolean deadline, boolean startdate, boolean finishdate, boolean notStarted, boolean inProgress, boolean finished, boolean less, boolean greater, LocalDate deadlineFirst, LocalDate deadlineLast, LocalDate startdateFirst, LocalDate startdateLast, LocalDate finishdateFirst, LocalDate finishdateLast, int priorityNumber, ArrayList<Category> categories){
        filter.status = status;
        filter.priority = priority;
        filter.category = category;
        filter.deadline = deadline;
        filter.startdate = startdate;
        filter.finishdate = finishdate;
        filter.notStarted = notStarted;
        filter.inProgress = inProgress;
        filter.finished = finished;
        filter.less = less;
        filter.greater = greater;
        filter.deadlineFirst = deadlineFirst;
        filter.deadlineLast = deadlineLast;
        filter.startdateFirst = startdateFirst;
        filter.startdateLast = startdateLast;
        filter.finishdateFirst = finishdateFirst;
        filter.finishdateLast = finishdateLast;
        filter.priorityNumber = priorityNumber;
        filter.categories = categories;
    }

    public boolean isStatus() {
        return status;
    }

    public boolean isPriority() {
        return priority;
    }

    public boolean isCategory() {
        return category;
    }

    public boolean isDeadline() {
        return deadline;
    }

    public boolean isStartdate() {
        return startdate;
    }

    public boolean isFinishdate() {
        return finishdate;
    }

    public boolean isNotStarted() {
        return notStarted;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isLess() {
        return less;
    }

    public boolean isGreater() {
        return greater;
    }

    public LocalDate getDeadlineFirst() {
        return deadlineFirst;
    }

    public LocalDate getDeadlineLast() {
        return deadlineLast;
    }

    public LocalDate getStartdateFirst() {
        return startdateFirst;
    }

    public LocalDate getStartdateLast() {
        return startdateLast;
    }

    public LocalDate getFinishdateFirst() {
        return finishdateFirst;
    }

    public LocalDate getFinishdateLast() {
        return finishdateLast;
    }

    public int getPriorityNumber() {
        return priorityNumber;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
