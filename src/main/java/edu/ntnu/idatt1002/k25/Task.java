package edu.ntnu.idatt1002.k25;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Random;

/**
 * The class for a single task.
 *
 * @author Kim Johnstuen Rokling
 * @author Gaute Degré Lorentsen
 * @version 1.0.1 2021.03.29
 * @since 2021.03.11
 */
public class Task implements Serializable {

    private Random random = new Random();
    private int taskID;
    private String taskName;
    private LocalDate deadline;
    private LocalDate startDate;
    private LocalDate finishDate;
    private String status;
    private String description;
    private byte priority;
    private Category category;

    public Task(String taskName, LocalDate deadline, LocalDate startDate, LocalDate finishDate, String status, String description, byte priority, Category category) {
        this.taskID = random.nextInt();
        this.taskName = taskName;
        this.deadline = deadline;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.status = status;
        this.description = description;
        this.priority = priority;
        this.category = category;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public byte getPriority() {
        return priority;
    }

    public Category getCategory() {
        return category;
    }

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * Equals method for task.
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return taskID == task.taskID &&
                priority == task.priority &&
                Objects.equals(deadline, task.deadline) &&
                Objects.equals(startDate, task.startDate) &&
                Objects.equals(finishDate, task.finishDate) &&
                Objects.equals(status, task.status) &&
                Objects.equals(description, task.description) &&
                Objects.equals(category, task.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskID, deadline, startDate, finishDate, status, description, priority, category);
    }

    @Override
    public String toString() {
        return "Task{" +
                "IDTask=" + taskID +
                ", taskName='" + taskName + '\'' +
                ", deadline=" + deadline +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", category=" + category +
                '}';
    }
}
