package edu.ntnu.idatt1002.k25;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.awt.Dimension;

import java.time.LocalDate;


/**
 *  Main.java
 *
 * @author Mikkel Ofrim
 * @author Gaute Degré Lorentsen
 * @author Kim Johnstuen Rokling
 * @version 1.2.1 2021.04.09
 * @since 2021.03.25
 */
public class Main extends Application{
    public void start(Stage stage) throws Exception {
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth()/1.2;
        double height = screenSize.getHeight()/1.35;
        Parent root = FXMLLoader.load(getClass().getResource("/HomeScreen.fxml")); //Change to HomeScreen.fxml
        stage.setTitle("My to-do list");
        stage.setResizable(false);
        stage.setScene(new Scene(root, width, height));
        stage.show();
    }

    public static void main(String[] args) throws Exception {
        launch(args);
    }
}
