package edu.ntnu.idatt1002.k25;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


public class taskTest {

    @Test
    @DisplayName("Test for adding task to task register")
    public void addingTasktoRegisterTest() throws Exception {
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name",  LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);

        Assertions.assertEquals(1, TaskRegister.getTasks().size());
    }

    @Test
    @DisplayName("Adding a Task")
    public void checksIfTaskIsInListAfterAddingATaskToTaskRegister() throws Exception {
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name",  LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);
        Assertions.assertTrue(TaskRegister.getTasks().contains(task));
    }

    @Test
    @DisplayName("Throwing argument when trying to add task already in register.")
    public void throwingArgumentWhenTryingToAddTaskAlreadyInRegister() throws Exception{
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name", LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);
        Assertions.assertThrows(Exception.class, () -> {
            TaskRegister.addTask(task);
        });
    }

    @Test
    @DisplayName("Deleting a Task")
    public void checksIfTaskIsInListAfterDeletingTaskFromTaskRegister() throws Exception {
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name",  LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);
        TaskRegister.deleteTask(task);
        Assertions.assertFalse(TaskRegister.getTasks().contains(task));
    }

    @Test
    @DisplayName("Test for deleting task from task register")
    public void deletingTaskFromTaskRegisterTest() throws Exception {
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name", LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);
        int taskListSize = TaskRegister.getTasks().size();
        TaskRegister.deleteTask(task);
        Assertions.assertEquals(taskListSize-1, TaskRegister.getTasks().size());
    }

    @Test
    @DisplayName("Throwing argument when trying to delete task not in register.")
    public void throwingArgumentWhenTryingToDeleteTaskNotInRegister() throws Exception{
        TaskRegister.getTasks().clear();
        Category category = new Category("category");
        Task task = new Task("name",LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        Task task2 = new Task("name2", LocalDate.of(2021,4,4), LocalDate.now(), LocalDate.of(2021, 4, 5), "Active", "sfsdf", Byte.valueOf("1"),category);
        TaskRegister.addTask(task);
        Assertions.assertThrows(Exception.class, () -> {
            TaskRegister.deleteTask(task2);
        });
    }
}


