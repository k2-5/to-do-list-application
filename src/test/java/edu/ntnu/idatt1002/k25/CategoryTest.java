package edu.ntnu.idatt1002.k25;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CategoryTest {

    @Test
    @DisplayName("Adding a new category")
    public void addingCategoryToCategoryRegister() throws Exception {
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        CategoryRegister.addCategory(category);
        Assertions.assertEquals(1, CategoryRegister.getCategories().size());
    }

    @Test
    @DisplayName("Throwing an argument when category already are registered")
    public void throwArgumentIfCategoryAlreadyInRegister() throws Exception{
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        CategoryRegister.addCategory(category);
        Assertions.assertThrows(Exception.class, () -> {
            CategoryRegister.addCategory(category);
        });
    }

    @Test
    @DisplayName("Adding a category")
    public void checksIfCategoryIsInListAfterAddingCategoryToCategoryRegister() throws Exception {
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        CategoryRegister.addCategory(category);
        Assertions.assertTrue(CategoryRegister.getCategories().contains(category));
    }

    @Test
    @DisplayName("Deleting a category")
    public void deletingCategoryFromCategoryRegister() throws Exception {
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        CategoryRegister.addCategory(category);
        int sizeList = CategoryRegister.getCategories().size();
        CategoryRegister.deleteCategory(category);
        Assertions.assertEquals(sizeList-1, CategoryRegister.getCategories().size());
    }


    @Test
    @DisplayName("Deleting a category")
    public void checksIfCategoryIsInListAfterDeletingCategoryFromCategoryRegister() throws Exception {
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        CategoryRegister.addCategory(category);
        CategoryRegister.deleteCategory(category);
        Assertions.assertFalse(CategoryRegister.getCategories().contains(category));
    }

    @Test
    @DisplayName("Throwing an argument when trying to remove category not in list")
    public void throwArgumentIfCategoryIsNotInRegister() throws Exception{
        CategoryRegister.getCategories().clear();
        Category category = new Category("category");
        Category category1 = new Category("Yee");
        CategoryRegister.addCategory(category);
        Assertions.assertThrows(Exception.class, () -> {
            CategoryRegister.deleteCategory(category1);
        });
    }
}
